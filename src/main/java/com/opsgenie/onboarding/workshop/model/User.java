package com.opsgenie.onboarding.workshop.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("name")
    private String name;
}
