package com.opsgenie.onboarding.workshop.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Team implements Serializable {

    public Team(String id, String teamName, String[] userIds) {
        this.id = id;
        this.teamName = teamName;
        this.userIds = userIds;
    }

    public Team() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String[] getUserIds() {
        return userIds;
    }

    public void setUserIds(String[] userIds) {
        this.userIds = userIds;
    }

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String teamName;
    @JsonProperty("userIds")
    private String[] userIds;


}
