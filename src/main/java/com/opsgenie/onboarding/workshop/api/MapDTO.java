package com.opsgenie.onboarding.workshop.api;

import com.opsgenie.onboarding.workshop.model.Team;


public class MapDTO {

    public TeamDTO teamToTeamDTO(Team team)
    {
        return new TeamDTO(team.getId(),team.getTeamName(),team.getUserIds());
    }

    public Team teamDTOToTeam(TeamDTO teamDTO)
    {
        return new Team(teamDTO.getId(),teamDTO.getTeamName(),teamDTO.getUserIds());
    }


}
