package com.opsgenie.onboarding.workshop.api;

import com.fasterxml.jackson.annotation.JsonProperty;
public class UserDTO {
    @JsonProperty("userID")
    private String userID;

    @JsonProperty("name")
    private String name;

    public UserDTO() {
    }
}
