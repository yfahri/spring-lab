package com.opsgenie.onboarding.workshop.repository;


import com.opsgenie.onboarding.workshop.api.TeamDTO;

import java.util.List;

public interface TeamRepository {

    public TeamDTO getTeam(String id);
    public List<TeamDTO> getAllTeams();
    public TeamDTO createTeam(TeamDTO teamDTO);
    public TeamDTO updateTeam(String id , TeamDTO teamDTO);
    public void deleteTeam(String id);
}
