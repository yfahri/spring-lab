package com.opsgenie.onboarding.workshop.repository;

import com.opsgenie.onboarding.workshop.api.MapDTO;
import com.opsgenie.onboarding.workshop.api.TeamDTO;
import com.opsgenie.onboarding.workshop.model.Team;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TeamRepositoryImpl implements TeamRepository {

    MapDTO mapDTO = new MapDTO();

    @Override
    public TeamDTO createTeam(TeamDTO teamDTO)
    {
        Team tmp = mapDTO.teamDTOToTeam(teamDTO);

        return null;
    }

    @Override
    public TeamDTO getTeam(String id) {
        return null;
    }

    @Override
    public List<TeamDTO> getAllTeams() {
        return null;
    }

    @Override
    public TeamDTO updateTeam(String id, TeamDTO teamDTO) {
        return null;
    }

    @Override
    public void deleteTeam(String id) {

    }
}
