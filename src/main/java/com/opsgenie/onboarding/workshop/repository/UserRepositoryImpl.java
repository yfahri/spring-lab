package com.opsgenie.onboarding.workshop.repository;


import com.opsgenie.onboarding.workshop.api.UserDTO;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {


    @Override
    public UserDTO createUser(UserDTO userDTO) {
        return null;
    }

    @Override
    public UserDTO updateUser(String id, UserDTO userDTO) {
        return null;
    }

    @Override
    public UserDTO getUser(String id) {
        return null;
    }

    @Override
    public void deleteUser(String id) {

    }
}
