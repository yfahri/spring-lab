package com.opsgenie.onboarding.workshop.repository;

import com.opsgenie.onboarding.workshop.api.UserDTO;

public interface UserRepository {
    public UserDTO createUser(UserDTO userDTO);
    public UserDTO updateUser(String id,UserDTO userDTO);
    public UserDTO getUser(String id);
    public void deleteUser(String id);
}
