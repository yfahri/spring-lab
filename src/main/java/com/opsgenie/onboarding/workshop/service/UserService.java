package com.opsgenie.onboarding.workshop.service;

import com.opsgenie.onboarding.workshop.api.UserDTO;

public interface UserService {

    public UserDTO getUser(String id);
    public UserDTO updateUser(String id,UserDTO userDTO);
    public void deleteUser(String id);
    public UserDTO createUser(UserDTO userDTO);
}
