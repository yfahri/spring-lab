package com.opsgenie.onboarding.workshop.service;

import com.opsgenie.onboarding.workshop.api.MapDTO;
import com.opsgenie.onboarding.workshop.api.TeamDTO;
import com.opsgenie.onboarding.workshop.model.Team;
import com.opsgenie.onboarding.workshop.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeamServiceImpl implements TeamService{


    private final TeamRepository teamRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    MapDTO mapDTO = new MapDTO();

    @Override
    public List<TeamDTO> listTeams() {
        List<TeamDTO> list = new ArrayList<>();
        list.add(mapDTO.teamToTeamDTO(new Team("1","team1",null)));
        list.add(mapDTO.teamToTeamDTO(new Team("2","team2",null)));
        return teamRepository.getAllTeams();
        //return list;
    }

    @Override
    public TeamDTO getTeam(String id) {

        return teamRepository.getTeam(id);
    }

    @Override
    public TeamDTO updateTeam(String id,TeamDTO teamDTO) {
        teamDTO.setTeamName("UPDATED");

        //repo dtoToTeam update -> teamToDTO
        return teamRepository.updateTeam(id,teamDTO);
    }

    @Override
    public TeamDTO createTeam(TeamDTO teamDTO)
    {
        //teamrepo create team

        return teamRepository.createTeam(teamDTO);
    }

    @Override
    public void deleteTeam(String id) {
        teamRepository.deleteTeam(id);

    }
}
