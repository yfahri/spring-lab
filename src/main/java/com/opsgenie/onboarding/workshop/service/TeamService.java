package com.opsgenie.onboarding.workshop.service;

import com.opsgenie.onboarding.workshop.api.TeamDTO;
import com.opsgenie.onboarding.workshop.model.Team;

import java.util.List;

public interface TeamService {
    public List<TeamDTO> listTeams();
    public TeamDTO getTeam(String id);
    public TeamDTO updateTeam(String id,TeamDTO team);
    public TeamDTO createTeam(TeamDTO teamDTO);
    public void deleteTeam(String id);

}
