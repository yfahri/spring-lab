package com.opsgenie.onboarding.workshop.service;

import com.opsgenie.onboarding.workshop.api.UserDTO;
import com.opsgenie.onboarding.workshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO getUser(String id) {
        return userRepository.getUser(id);
    }

    @Override
    public UserDTO updateUser(String id,UserDTO userDTO) {
        return userRepository.updateUser(id,userDTO);
    }

    @Override
    public void deleteUser(String id) {

        userRepository.deleteUser(id);
    }

    @Override
    public UserDTO createUser(UserDTO userDTO) {
        return userRepository.createUser(userDTO);
    }
}
