package com.opsgenie.onboarding.workshop.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class HelloController {

    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    public ResponseEntity<String> getHello()
    {
        return ResponseEntity.ok("Hello");
    }
}
