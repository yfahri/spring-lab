package com.opsgenie.onboarding.workshop.controller;


import com.opsgenie.onboarding.workshop.model.Team;
import com.opsgenie.onboarding.workshop.service.TeamService;
import com.opsgenie.onboarding.workshop.api.TeamDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/teams")
public class TeamController {

    @Autowired
    TeamService teamService;

    @RequestMapping(value = "/teams",method = RequestMethod.GET)
    public List<TeamDTO> getAllTeams()
    {
        return teamService.listTeams();
    }

    @RequestMapping(value = "/teams",method = RequestMethod.POST)
    public TeamDTO createTeam(@RequestBody TeamDTO team)
    {
        return teamService.createTeam(team);
    }

    @RequestMapping(value = "/teams/{id}",method = RequestMethod.PUT)
    public TeamDTO updateTeam(@PathVariable String id ,@RequestBody TeamDTO team)
    {
        return teamService.updateTeam(id,team);
    }

    @RequestMapping(value = "/teams/{id]",method = RequestMethod.GET)
    public TeamDTO getTeams(@PathVariable String id)
    {
        return teamService.getTeam(id);
    }

    @RequestMapping(value = "/teams/{id}",method = RequestMethod.DELETE)
    public void deleteTeam(@PathVariable String id)
    {

        teamService.deleteTeam(id);

    }
}
