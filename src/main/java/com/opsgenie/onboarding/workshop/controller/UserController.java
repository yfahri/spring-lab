package com.opsgenie.onboarding.workshop.controller;


import com.opsgenie.onboarding.workshop.api.UserDTO;
import com.opsgenie.onboarding.workshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/user",method = RequestMethod.POST)
    public UserDTO createUser(@RequestBody UserDTO userDTO)
    {
        //return userRepository.createUser(userDTO);
        return userService.createUser(userDTO);
    }
    @RequestMapping(value = "/user/{id}",method = RequestMethod.GET)
    public UserDTO getUser(@PathVariable String id)
    {
        //return userRepository.createUser(userDTO);
        return userService.getUser(id);
    }

    @RequestMapping(value = "/user/{id}",method = RequestMethod.PUT)
    public UserDTO updateUser(@PathVariable String id,@RequestBody UserDTO userDTO)
    {
        //return userRepository.createUser(userDTO);
        return userService.updateUser(id,userDTO);
    }


    @RequestMapping(value = "/user/{id}",method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable String id)
    {
        //return userRepository.createUser(userDTO);

    }
}
